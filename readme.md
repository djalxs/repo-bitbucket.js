Repo-bitbucket.js
=======

Repo-bitbucket.js is a jQuery plugin that alows embed public bitbucket repository on your site. Repo-bitbucket.js is influenced by [Darcy Clarke](https://github.com/darcyclarke/Repo.js)'s repo.js.

Repo-bitbucket.js uses (like [repo.js]) [Markus Ekwall](https://twitter.com/#!/mekwall)'s [jQuery Vangogh](https://github.com/mekwall/jquery-vangogh) plugin for styling of file contents.


##Example Use

HTML:
	
	:::html
	<link href='http://fonts.googleapis.com/css?family=Cousine' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<link href="repo-bitbucket/repo-bitbucket.css" rel="stylesheet" type='text/css'>
	<script src="repo-bitbucket/repo-bitbucket.js" type="text/javascript"></script>


Javascript:
	
	:::javascript
	$('#repository').bitbucket({user : 'miroslavmagda', repository : 'repo-bitbucket.js', revision : 'master'});


You can also get a specific revision:

	:::javascript
	$('#repository').bitbucket({user : 'miroslavmagda', repository : 'repo-bitbucket.js', revision : '8a2143f0e994'});


![Test page][1]

@author [Miroslav Magda](http://ejci.net)

@version 0.9

##License
All code is open source and dual licensed under GPL and MIT. Check the individual licenses for more information.


[1]: http://cdn.bitbucket.org/miroslavmagda/repo-bitbucket.js/downloads/repo-bitbucket.js.png